//
//  DataModel.swift
//  Checklists
//
//  Created by Austin Brown on 2/4/19.
//  Copyright © 2019 The Oash. All rights reserved.
//

import Foundation

class DataModel {
    
    // MARK: Properties
    var lists = [Checklist]()
    // Computed property
    var indexOfSelectedChecklist: Int {
        get {
            return UserDefaults.standard.integer( forKey: "ChecklistIndex")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "ChecklistIndex")
        }
    }
    
    init() {
        loadChecklists()
        registerDefaults()
        handleFirstTime()
    }
    
    func documentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func dataFilePath() -> URL {
        return documentsDirectory().appendingPathComponent("Checklists.plist")
    }
    
    // this method is now called saveChecklists()
    func saveChecklists() {
        let encoder = PropertyListEncoder()
        do {
            // You encode lists instead of "items"
            let data = try encoder.encode(lists)
            let currentFilePath = dataFilePath()
            try data.write(to: currentFilePath, options: Data.WritingOptions.atomic)
        } catch {
            print("Error encoding list array: \(error.localizedDescription)")
        }
    }
    
    // this method is now called loadChecklists()
    func loadChecklists() {
        let path = dataFilePath()
        if let data = try? Data(contentsOf: path) {
            let decoder = PropertyListDecoder()
            do {
                // You decode to an object of [Checklist] type to lists
                lists = try decoder.decode([Checklist].self, from: data)
                sortChecklists()
            } catch {
                print("Error decoding list array: \(error.localizedDescription)")
            }
        }
    }
    
    // This creates a new Dictionary instance and adds the value -1 for the key "ChecklistIndex"
    // UserDefaults will use the values from this dictionary if you ask it for a key and it
    // cannot find a value for that key.
    func registerDefaults() {
        // This is because originally, there was one value in the dictionary and it was an Int.
        // But when you introduced the FirstTime key, its corresponding value is a Bool. Now your
        // dictionary has a mixed set of values — an Int and a Bool. So, at this point, the
        // compiler is unsure whether you meant to have a mixed bag of values, or if it was a
        // mistake on your part. So it wants you to explicitly indicate what the dictionary type
        // is, and that's why you declare it as [String: Any], to indicate that the value could
        // indeed be of any type.
        let dictionary = [ "ChecklistIndex": -1 , "FirstTime": true] as [String: Any]
        UserDefaults.standard.register(defaults: dictionary)
    }
    
    func handleFirstTime() {
        // Check UserDefaults for the value of the "FirstTime" key. If the value for "FirstTime"
        // is true, then this is the first time the app is being run. In that case, you create
        // a new Checklist object and add it to the array.
        let userDefaults = UserDefaults.standard
        let firstTime = userDefaults.bool(forKey: "FirstTime")
        
        if firstTime {
            let checklist = Checklist(name: "New List")
            lists.append(checklist)
            
            indexOfSelectedChecklist = 0
            userDefaults.set(false, forKey: "FirstTime")
            userDefaults.synchronize()
        }
    }
    
    func sortChecklists() {
        // lists is sorted by logic in closure
        lists.sort(by: { list1, list2 in
            // The localizedStandardCompare(_:) method compares the two name strings while
            // ignoring lowercase vs. uppercase (so “a” and “A” are considered equal) and taking
            // into consideration the rules of the current locale.
            // A locale is an object that knows about country and language-specific rules.
            // Sorting in German may be different than sorting in English, for example.
            return list1.name.localizedStandardCompare(list2.name) == .orderedAscending })
    }
}
