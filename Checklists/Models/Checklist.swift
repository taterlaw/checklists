//
//  Checklist.swift
//  Checklists
//
//  Created by Austin Brown on 1/25/19.
//  Copyright © 2019 The Oash. All rights reserved.
//

import UIKit

// Subclassed to be able to compare objects
class Checklist: NSObject, Codable {
    var name = ""
    // Default icon that is name 'No Icon'
    var iconName = "No Icon"
    var items = [ChecklistItem]()
    
    // Contains a default parameter value
    init(name: String, iconName: String = "No Icon") {
        self.name = name
        self.iconName = iconName
        super.init()
    }
    
    func countUncheckedItems() -> Int {
        var count = 0
        for item in items where !item.isChecked {
            count += 1
        }
        return count
    }
}
