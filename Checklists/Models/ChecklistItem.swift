//
//  ChecklistItem.swift
//  Checklists
//
//  Created by Austin Brown on 12/15/18.
//  Copyright © 2018 The Oash. All rights reserved.
//

import Foundation

// Inheritance was used for the need of equatable in a list of this type
// of object.
// Methods not implemented by class from protocol inheritance have default implementations
class ChecklistItem: NSObject, Codable {
    var text = ""
    var isChecked = false
    
    override init() {
        text = "Hello"
        isChecked = false
    }
    
    init(text: String, isChecked: Bool) {
        self.text = text
        self.isChecked = isChecked
    }
    
    func ToggleChecked() -> Void {
        isChecked = !isChecked
    }
}
