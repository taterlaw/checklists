//
//  AddItemTableViewController.swift
//  Checklists
//
//  Created by Austin Brown on 12/18/18.
//  Copyright © 2018 The Oash. All rights reserved.
//

import UIKit

/*
Delegates in five easy steps!!!!!!!!!!!!!!!
 
These are the steps for setting up the delegate pattern between two objects, where object A is
the delegate for object B, and object B will send messages back to A. The steps are:
1 - Define a delegate protocol for object B.
2 - Give object B an optional delegate variable. This variable should be weak.
3 - Update object B to send messages to its delegate when something interesting happens,
    such as the user pressing the Cancel or Done buttons, or when it needs a piece of information.
    You write delegate?.methodName(self, . . .)
4 - Make object A conform to the delegate protocol. It should put the name of the protocol
    in its class line and implement the methods from the protocol.
5 - Tell object B that object A is now its delegate. (Done in prepare-for-segue)
*/
protocol ItemDetailViewControllerDelegate: class {
    // When the user presses CANCEL.
    func itemDetailViewControllerDidCancel(_ controller: ItemDetailViewController)
    
    // When the user presses DONE.
    func itemDetailViewController(_ controller: ItemDetailViewController,
        didFinishAdding item: ChecklistItem)
    
    // For updating existing items in the data model
    func itemDetailViewController(_ controller: ItemDetailViewController,
        didFinishEditing item: ChecklistItem)
}

// Can be a delegate for textfield objects
class ItemDetailViewController: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var doneBarButton: UIBarButtonItem!
    
    weak var delegate: ItemDetailViewControllerDelegate?
    var itemToEdit: ChecklistItem?
    
    // Called when the view controller is loaded, but before it is shown on screen
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .never
        
        // Added to be able to edit existing items instead of adding items
        // itemToEdit = itemToEdit: This is "variable shadowing" when using optionals
        if let itemToEdit = itemToEdit {
            title = "Edit Item"
            textField.text = itemToEdit.text
            doneBarButton.isEnabled = true
        }
    }
    
    // When the user taps on a cell, the table view sends the delegate a willselectrowat message
    // that says hi delegate, I am about to select this particular row
    // returning nil the delegate answers sorry you cannot select this row
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
    
    // Causes the keyboard to automatically appear when the screen opens.
    // The view controller receives the viewWillAppear() message just before it
    // becomes visible.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        textField.becomeFirstResponder()
    }
    
    @IBAction func Cancel(_ sender: Any) {
        //navigationController?.popViewController(animated: true)
        delegate?.itemDetailViewControllerDidCancel(self)
    }
    
    @IBAction func Done(_ sender: Any) {
        if let itemToEdit = itemToEdit {
            // Editing an existing item in the list
            itemToEdit.text = textField.text!
            delegate?.itemDetailViewController(self, didFinishEditing: itemToEdit)
        } else {
            // Adding a new item to the list
            //print("Contents of the text field: \(textField.text!)")
            //navigationController?.popViewController(animated: true)
            let item = ChecklistItem()
            item.text = textField.text!
            
            delegate?.itemDetailViewController(self, didFinishAdding: item)
        }
    }
    
    // MARK:- Text Field Delegates
    // This is one of the UITextField delegate methods. It is invoked every time the user
    // changes the text, whether by tapping on the keyboard or via cut/paste.
    // This method does not give new text just what range of text should be replaced and
    // with what text should do the replacing
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        // Figure out what the new text will be
        let oldText = textField.text!
        let stringRange = Range(range, in:oldText)!
        let newText = oldText.replacingCharacters(in: stringRange, with: string)
        
        doneBarButton.isEnabled = !newText.isEmpty
        return true
    }
    
    // This delegate method is called when the user taps the clear button on keyboard
    // for a text field entry. This disables the done button because this method
    // indicates there is no text to read.
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        doneBarButton.isEnabled = false
        return true
    }
}
