//
//  ViewController.swift
//  Checklists
//
//  Created by Austin Brown on 11/16/18.
//  Copyright © 2018 The Oash. All rights reserved.
//

import UIKit

class ChecklistViewController: UITableViewController, ItemDetailViewControllerDelegate {
    
    // MARK: Properties
    var checklist: Checklist!
    
    // MARK: Delegate Functions
    func itemDetailViewControllerDidCancel(_ controller: ItemDetailViewController) {
        navigationController?.popViewController(animated:true)
    }
    
    // Add new to-do items
    func itemDetailViewController(_ controller: ItemDetailViewController,
                               didFinishAdding item: ChecklistItem) {
        // Update data model
        let newRowIndex = checklist.items.count
        checklist.items.append(item)
        
        // Updates the view for the newly added item
        let indexPath = IndexPath(row: newRowIndex, section: 0)
        let indexPaths = [indexPath]
        tableView.insertRows(at: indexPaths, with: .automatic)
        navigationController?.popViewController(animated:true)
    }
    
    func itemDetailViewController(_ controller: ItemDetailViewController,
                               didFinishEditing item: ChecklistItem) {
        if let index = checklist.items.firstIndex(of: item) {
            let indexPath = IndexPath(row: index, section: 0)
            if let cell = tableView.cellForRow(at: indexPath) {
                configureText(for: cell, with: item)
            }
        }
        navigationController?.popViewController(animated:true)
    }
    
    // MARK:- Navigation
    /*
     Prepare-For-Segue
     Invoked by UIKit when a segue from one screen to another is about to be performed.
     Recall that the segue is the arrow between two view controllers in the storyboard.
     
     Using prepare-for-segue allows you to pass data to the new view controller before
     it is displayed. Usually you’ll do this by setting one or more of the new view
     controller's properties.
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /*
         Because there may be more than one segue per view controller, it’s a good idea
         to give each segue a unique identifier and to check for that identifier first to
         make sure you’re handling the correct segue.
        */
        if segue.identifier == "AddItem" {
        
            /*
             The new view controller to be displayed can be found in segue.destination,
             but destination is of type UIViewContoller since the new view controller could
             be any view controller sub-class.
             
             So, you cast destination to AddItemViewController to get a reference to an object
             with the right type. (The as! keyword is known as a type cast or a force
             downcast since you are casting an object of one type to a different type.
             Do note that if you downcast objects of completely different types, you might
             get a nil value. The casting works here because AddItemViewController is a
             sub-class of UIViewContoller.)
            */
            let controller = segue.destination as! ItemDetailViewController
            
            /*
             Once you have a reference to the AddItemViewController object, you set its delegate
             property to self and the connection is complete. This tells AddItemViewController
             that from now on, the object identified as self is its delegate. But what is “self”
             here? Well, since you’re editing ChecklistViewController.swift, self refers to
             ChecklistViewController.
            */
            controller.delegate = self
        } else if segue.identifier == "EditItem" {
            let controller = segue.destination as! ItemDetailViewController
            controller.delegate = self
            
            if let indexPath = tableView.indexPath(for: sender as! UITableViewCell) {
                controller.itemToEdit = checklist.items[indexPath.row]
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        //navigationController?.navigationBar.prefersLargeTitles = true
        
        // Disable large titles for this view controller
        navigationItem.largeTitleDisplayMode = .never
        title = checklist.name
    }
    
    // This tells the table view that you have just x rows of data
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checklist.items.count
    }
    
    // Obtains a cell for that row. Grabs a copy of the prototype cell and gives that back to the
    // table view. Normally you would put the row data into the cell here as well.
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // 1. Add a prototype cell to the table view in the storyboard.
        // 2. Set a resuse identifier on the prototype cell.
        // 3. Call tableView.dequeueReusableCell(withIdentifier:for:). This makes a new copy of the
        //    prototype cell if necessary, or recycles an existing cell that is no longer in use.
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChecklistItem", for: indexPath)
        
        // Ask the table cell for the view with the tag 1000. Returns a ref to the corresponding
        // UILabel
        //let label = cell.viewWithTag(1000) as! UILabel
        
        // indexPath is an object that points to a specific row in the table. When the table view
        // asks the data source for a cell you can look at the row number inside the indexPath.row
        // property to find out the row for which the cell is intended
        let item = checklist.items[indexPath.row]
        configureText(for: cell, with: item)
        configureCheckmark(for: cell, with: item)
        return cell
    }
    
    // Gets called whenever the user taps on a cell
    // Used to toggle checkmark
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            let item = checklist.items[indexPath.row]
            item.ToggleChecked()
            configureCheckmark(for: cell, with: item)
        }
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    // Delete item from checklist
    override func tableView(_ tableView: UITableView,
                            commit editingStyle: UITableViewCell.EditingStyle,
                            forRowAt indexPath:IndexPath) {
        checklist.items.remove(at: indexPath.row)
        let indexPaths = [indexPath]
        tableView.deleteRows(at: indexPaths, with: .automatic)
        
    }
    
    func configureCheckmark(for cell: UITableViewCell, with item: ChecklistItem) {
        let label = cell.viewWithTag(1001) as! UILabel
        
        if item.isChecked {
            label.text = "√"
        } else {
            label.text = ""
        }
    }
    
    func configureText(for cell: UITableViewCell, with item: ChecklistItem) {
        let label = cell.viewWithTag(1000) as! UILabel
        label.text = item.text
    }
    
    
    
    
    
    
    
    
    
    /*
    @IBAction func AddItem(_ sender: Any) {
        // Index of the new row in the array
        let newRowIndex = items.count
        let item = ChecklistItem(text: "I am a new row", isChecked: false)
        // Add new item to data model
        items.append(item)
        // Tell the table view about new row so it can add a new cell for that row
        // IndexPath object that points to the new row
        let indexPath = IndexPath(row: newRowIndex, section: 0)
        // Temp array holding just the one index path item
        let indexPaths = [indexPath]
        // Tell the table view about the new row
        tableView.insertRows(at: indexPaths, with: .automatic)
        // If the item was not added to both the data model and table view the app will crash
        // because these always have to be in sync.
    }
    */
}
