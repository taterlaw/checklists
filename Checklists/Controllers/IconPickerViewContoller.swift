//
//  IconPickerViewContoller.swift
//  Checklists
//
//  Created by Austin Brown on 2/13/19.
//  Copyright © 2019 The Oash. All rights reserved.
//

import Foundation
import UIKit

protocol IconPickerViewControllerDelegate: class {
    func iconPicker(_ picker: IconPickerViewController, didPick iconName: String)
}

// Defines the IconPickerViewController object, which is a table view controller, and a
// delegate protocol that it uses to communicate with other objects in the app.
class IconPickerViewController: UITableViewController {
    weak var delegate: IconPickerViewControllerDelegate?
    
    // Contains a list of icon names. These strings are both the text you will show on the
    // screen and the name of the PNG file inside the asset catalog.
    let icons = [ "No Icon", "Appointments", "Birthdays", "Chores",
                  "Drinks", "Folder", "Groceries", "Inbox", "Photos", "Trips" ]
    
    // MARK:- Table View Delegates
    // Returns the number of icons in the array.
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return icons.count
    }
    
    // Obtain a table view cell and give it a title and an image. It will be a prototype
    // cell with the “default” cell style (or “Basic” as it is called in Interface Builder).
    // Cells with this style already contain a text label and an image view, which is
    // very convenient.
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "IconCell", for: indexPath)
            let iconName = icons[indexPath.row]
            cell.textLabel!.text = iconName
            cell.imageView!.image = UIImage(named: iconName)
            return cell
    }
    
    override func tableView(_ tableView: UITableView,
                               didSelectRowAt indexPath: IndexPath) {
        if let delegate = delegate {
            let iconName = icons[indexPath.row]
            delegate.iconPicker(self, didPick: iconName)
        }
    }
}
