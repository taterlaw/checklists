//
//  AllListsViewController.swift
//  Checklists
//
//  Created by Austin Brown on 1/9/19.
//  Copyright © 2019 The Oash. All rights reserved.
//

import UIKit

class AllListsViewController: UITableViewController {
    
    // MARK: Properties
    let cellIdentifier = "ChecklistCell"
    // ! is necessary because dataModel will temporarily be nil when the app starts up. It
    // doesnʼt have to be a true optional – with ? – because once dataModel is given a value,
    // it will never become nil again.
    var dataModel: DataModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    // Called before viewDidAppear(), when the view is about to become visible but the
    // animation hasn’t started yet.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    // UIKit automatically calls this method after the view controller becomes visible.
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Every view controller has a built-in navigationController property. To access
        // its delegate property you use the notation navigationController?.delegate because
        // the navigation controller is optional.
        navigationController?.delegate = self
        
        // Load the last opened user Checklist if possible.
        let index = dataModel.indexOfSelectedChecklist
        if index >= 0 && index < dataModel.lists.count {
            let checklist = dataModel.lists[index]
            performSegue(withIdentifier: "ShowChecklist", sender: checklist)
        }
    }

    // Removed the numbersOfSections(in:)
    // The view will now default to a single section in the table view
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.lists.count
    }

    // Get a cell and update attributes
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Define a constant to hold the newly created cell and then see if you can dequeue
        // a cell from the table view for the given identfier. If there is no cell — meaning
        // that there are no cached cells that can be re-used — then you create a new
        // UITableViewCell instance with the cell style, and the identifier, that you want.
        // If there is a cell, then you assign its reference to the previously declared constant.
        let cell: UITableViewCell!
        if let c = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) {
            cell = c
        } else {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
        
        cell.textLabel!.text = dataModel.lists[indexPath.row].name
        cell.accessoryType = .detailDisclosureButton
        
        // Check if any more to-do items
        let count = dataModel.lists[indexPath.row].countUncheckedItems()
        if dataModel.lists.count == 0 {
            cell.detailTextLabel!.text = "No Items"
        } else {
            cell.detailTextLabel!.text = count == 0 ? "All Done" : "\(count) Remaining"
        }
        
        cell.imageView!.image = UIImage(named: dataModel.lists[indexPath.row].iconName)
        return cell
    }
    
    // Performing a seque programatically when the user taps a row
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Store the index of the selected row into UserDefaults under the key "ChecklistIndex"
        dataModel.indexOfSelectedChecklist = indexPath.row
        
        let checklist = dataModel.lists[indexPath.row]
        performSegue(withIdentifier: "ShowChecklist", sender: checklist)
    }
    
    // Allows the user to delete checklists
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        dataModel.lists.remove(at: indexPath.row)
        
        let indexPaths = [indexPath]
        tableView.deleteRows(at: indexPaths, with: .automatic)
    }
    
    // You create the view controller object for the Add/Edit Checklist screen and push it
    // on to the navigation stack. This is roughly equivalent to what a segue would do behind
    // the scenes. The view controller is embedded in a storyboard and you have to ask the
    // storyboard object to load it.
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        // Each view controller has a storyboard property that refers to the storyboard the view
        // controller was loaded from. You can use that property to do all kinds of things with
        // the storyboard, such as instantiating other view controllers.
        let controller =
            storyboard!.instantiateViewController(withIdentifier: "ListDetailViewController") as! ListDetailViewController
        controller.delegate = self
        
        let checklist = dataModel.lists[indexPath.row]
        controller.checklistToEdit = checklist
        
        navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowChecklist" {
            let controller = segue.destination as! ChecklistViewController
            controller.checklist = sender as? Checklist
        } else if segue.identifier == "AddChecklist" {
            let controller = segue.destination as! ListDetailViewController
            controller.delegate = self
        }
    }
}

// MARK: AllListsViewController class delegates
// Handles the delegate calls from other classes.
extension AllListsViewController: ListDetailViewControllerDelegate {
    
    func listDetailViewControllerDidCancel(_ controller: ListDetailViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    func listDetailViewController(_ controller: ListDetailViewController, didFinishAdding checklist: Checklist) {
        dataModel.lists.append(checklist)
        dataModel.sortChecklists()
        // Getting away with reload because there are a handful of items that will go in list
        tableView.reloadData()
        navigationController?.popViewController(animated: true)
    }
    
    func listDetailViewController(_ controller: ListDetailViewController, didFinishEditing checklist: Checklist) {
        dataModel.sortChecklists()
        // Getting away with reload because there are a handful of items that will go in list
        tableView.reloadData()
        navigationController?.popViewController(animated: true)
    }
}

// To be notified when the user presses the back button on the navigation bar, you have to
// become a delegate of the navigation controller. Being the delegate means that the navigation
// controller tells you when it pushes or pops view controllers on the navigation stack.
extension AllListsViewController: UINavigationControllerDelegate {
    // MARK:- Navigation Controller Delegates
    // This method is called whenever the navigation controller shows a new screen.
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        /*
        // Was the back button tapped?
        // "===" checks whether two variables refer to the exact same object
        if viewController === self {
            // If the back button was pressed, the new view controller is AllListsViewController
            // itself and you set the “ChecklistIndex” value in UserDefaults to -1, meaning that
            // no checklist is currently selected.
            UserDefaults.standard.set(-1, forKey: "ChecklistIndex")
        }
        */
        if viewController === self {
            dataModel.indexOfSelectedChecklist = -1
        }
    }
}
